
LIBNAME     = opencm3_stm32f4
DEFS        += -DSTM32F4

FP_FLAGS    ?= -msoft-float
ARCH_FLAGS = -mthumb -mcpu=cortex-m4 $(FP_FLAGS)


all: controller

.PHONY: libopencm3 all clean controller flash

controller: libopencm3
	$(MAKE) -C src

libopencm3:
	$(MAKE) -C libopencm3

clean:
	$(MAKE) -C libopencm3 $@
	$(MAKE) -C src $@

flash: controller
	$(MAKE) -C src $@
