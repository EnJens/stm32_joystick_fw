#ifndef _COMMON_H_
#define _COMMON_H_

#include <stdint.h>

typedef struct
{
    char *name;
    uint32_t port;
    uint16_t pin;
    uint32_t rccport;
} pin_def_t;

#define PIN_DEF(name, port, pin)  [name] = { #name, port, pin, RCC_ ## port }

enum
{
    PIN_P1_UP = 0,
    PIN_P1_DOWN,
    PIN_P1_LEFT,
    PIN_P1_RIGHT,
    PIN_P1_BTN1,
    PIN_P1_BTN2,
    PIN_P1_BTN3,
    PIN_P1_BTN4,
    PIN_P1_BTN5,
    PIN_P1_BTN6,
    PIN_P1_SELECT,
    PIN_P1_START,

    PIN_P2_UP,
    PIN_P2_DOWN,
    PIN_P2_LEFT,
    PIN_P2_RIGHT,
    PIN_P2_BTN1,
    PIN_P2_BTN2,
    PIN_P2_BTN3,
    PIN_P2_BTN4,
    PIN_P2_BTN5,
    PIN_P2_BTN6,
    PIN_P2_SELECT,
    PIN_P2_START,

    PIN_SPI2_CLK,
    PIN_SPI2_MOSI,
    PIN_LAST
};

#endif /* _COMMON_H_ */
