#include <stdlib.h>
#include <string.h>
#include <libopencm3/stm32/rcc.h>
#include <libopencm3/stm32/gpio.h>
#include <libopencm3/stm32/spi.h>

#include "apa102.h"

int apa102_init(apa102_t *apa, uint32_t spi, pin_def_t clk, pin_def_t output)
{
    if(clk.port != output.port)
        return -1;

    rcc_periph_clock_enable(clk.rccport);
    rcc_periph_clock_enable(RCC_AFIO);
    rcc_periph_clock_enable(RCC_SPI2);

    gpio_set_mode(clk.port, GPIO_MODE_OUTPUT_50_MHZ,
        GPIO_CNF_OUTPUT_ALTFN_PUSHPULL, clk.pin | output.pin);

    spi_reset(spi);
    spi_init_master(
        spi, SPI_CR1_BAUDRATE_FPCLK_DIV_2, SPI_CR1_CPOL_CLK_TO_1_WHEN_IDLE,
        SPI_CR1_CPHA_CLK_TRANSITION_2, SPI_CR1_DFF_8BIT, SPI_CR1_MSBFIRST);
    spi_enable(spi);

    apa->spi = spi;
    apa->clk = clk;
    apa->output = output;
    apa->level = 1.0f;

    return 0;
}

// TODO: Error checking
static void spi_send_word(uint32_t spi, uint32_t value)
{
    spi_send(spi, (value >> 24) & 0xFF);
    spi_send(spi, (value >> 16) & 0xFF);
    spi_send(spi, (value >> 8) & 0xFF);
    spi_send(spi, (value) & 0xFF);
}

int apa102_draw(apa102_t *apa, const uint32_t *data, size_t length)
{
    spi_send_word(apa->spi, 0x00000000);

    const uint8_t level = 0xE0 | (uint8_t)(apa->level * 31);
    for(uint32_t i = 0; i < length; ++i)
        spi_send_word(apa->spi, level << 24 | data[i]);

    spi_send_word(apa->spi, 0xFFFFFFFF);

    return 0;
}

void apa102_setlevel(apa102_t *apa, float level)
{
    apa->level = level;
}
