#ifndef _APA102_H_
#define _APA102_H_

#include <stdint.h>
#include "common.h"

typedef struct {
    uint32_t spi;
    pin_def_t clk;
    pin_def_t output;
    float level;
} apa102_t;

int apa102_init(apa102_t *apa, uint32_t spi, pin_def_t clk, pin_def_t output);
int apa102_draw(apa102_t *apa, const uint32_t *data, size_t length);
void apa102_setlevel(apa102_t *apa, float level);
#endif /* _APA102_H_ */
