#include <stdlib.h>
#include <string.h>

#include <libopencm3/cm3/systick.h>
#include <libopencm3/stm32/rcc.h>
#include <libopencm3/stm32/gpio.h>
#include <libopencm3/stm32/spi.h>
#include <libopencm3/usb/usbd.h>
#include <libopencm3/usb/hid.h>

#include "common.h"
#include "apa102.h"

#define HID_ENDPOINT 0x81

static const pin_def_t pin_defs[PIN_LAST] =
{
    PIN_DEF(PIN_P1_UP, GPIOB, GPIO9),
    PIN_DEF(PIN_P1_DOWN, GPIOB, GPIO8),
    PIN_DEF(PIN_P1_LEFT, GPIOB, GPIO7),
    PIN_DEF(PIN_P1_RIGHT, GPIOB, GPIO6),
    PIN_DEF(PIN_P1_BTN1, GPIOB, GPIO5),
    PIN_DEF(PIN_P1_BTN2, GPIOD, GPIO2),
    PIN_DEF(PIN_P1_BTN3, GPIOC, GPIO12),
    PIN_DEF(PIN_P1_BTN4, GPIOC, GPIO11),
    PIN_DEF(PIN_P1_BTN5, GPIOC, GPIO10),
    PIN_DEF(PIN_P1_BTN6, GPIOA, GPIO8),
    PIN_DEF(PIN_P1_SELECT, GPIOC, GPIO9),
    PIN_DEF(PIN_P1_START, GPIOC, GPIO8),

    PIN_DEF(PIN_P2_UP, GPIOA, GPIO0),
    PIN_DEF(PIN_P2_DOWN, GPIOA, GPIO1),
    PIN_DEF(PIN_P2_LEFT, GPIOA, GPIO2),
    PIN_DEF(PIN_P2_RIGHT, GPIOA, GPIO3),
    PIN_DEF(PIN_P2_BTN1, GPIOA, GPIO4),
    PIN_DEF(PIN_P2_BTN2, GPIOA, GPIO5),
    PIN_DEF(PIN_P2_BTN3, GPIOA, GPIO6),
    PIN_DEF(PIN_P2_BTN4, GPIOA, GPIO7),
    PIN_DEF(PIN_P2_BTN5, GPIOC, GPIO4),
    PIN_DEF(PIN_P2_BTN6, GPIOC, GPIO5),
    PIN_DEF(PIN_P2_SELECT, GPIOB, GPIO0),
    PIN_DEF(PIN_P2_START, GPIOB, GPIO1),
    PIN_DEF(PIN_SPI2_CLK, GPIOB, GPIO13),
    PIN_DEF(PIN_SPI2_MOSI, GPIOB, GPIO15),
};

enum
{
    P1_REPORT_ID = 1,
    P2_REPORT_ID = 2
};

static uint8_t usbd_control_buffer[128];
static bool usb_ready = false;

static const struct usb_device_descriptor device_descriptor =
{
    .bLength = USB_DT_DEVICE_SIZE,
    .bDescriptorType = USB_DT_DEVICE,
    .bcdUSB = 0x0200,
    .bDeviceClass = 0,
    .bDeviceSubClass = 0,
    .bDeviceProtocol = 0,
    .bMaxPacketSize0 = 64,
    .idVendor = 0x0483,
    .idProduct = 0x5710,
    .bcdDevice = 0x0200,
    .iManufacturer = 1,
    .iProduct = 2,
    .iSerialNumber = 3,
    .bNumConfigurations = 1,
};

static const uint8_t hid_report_descriptor[] =
{
    0x05, 0x01, // USAGE_PAGE(Generic Desktop Controls)
    0x09, 0x04, // USAGE(Joystick)
    0xA1, 0x01, // COLLECTION(Application)
    0xA1, 0x00, //     COLLECTION(Physical)
    0x85, 0x01, //         REPORT_ID(1)
    0x05, 0x09, //         USAGE_PAGE(Button)
    0x19, 0x01, //         USAGE_MINIMUM(1)
    0x29, 0x10, //         USAGE_MAXIMUM(16)
    0x15, 0x00, //         LOGICAL_MINIMUM(0)
    0x25, 0x01, //         LOGICAL_MAXIMUM(1)
    0x95, 0x10, //         REPORT_COUNT(16)
    0x75, 0x01, //         REPORT_SIZE(1)
    0x81, 0x02, //         INPUT(Data, Var, Abs)
    0x05, 0x01, //         USAGE_PAGE(Generic Desktop Controls)
    0x09, 0x30, //         USAGE(X)
    0x09, 0x31, //         USAGE(Y)
    0x15, 0x81, //         LOGICAL_MINIMUM(-127)
    0x25, 0x7F, //         LOGICAL_MAXIMUM(127)
    0x75, 0x08, //         REPORT_SIZE(8)
    0x95, 0x02, //         REPORT_COUNT(2)
    0x81, 0x02, //         INPUT(Data, Var, Abs)
    0xC0,       //     END_COLLECTION
    0xC0,       // END_COLLECTION

    0x05, 0x01, // USAGE_PAGE(Generic Desktop Controls)
    0x09, 0x04, // USAGE(Joystick)
    0xA1, 0x01, // COLLECTION(Application)
    0xA1, 0x00, //     COLLECTION(Physical)
    0x85, 0x02, //         REPORT_ID(1)
    0x05, 0x09, //         USAGE_PAGE(Button)
    0x19, 0x01, //         USAGE_MINIMUM(1)
    0x29, 0x10, //         USAGE_MAXIMUM(16)
    0x15, 0x00, //         LOGICAL_MINIMUM(0)
    0x25, 0x01, //         LOGICAL_MAXIMUM(1)
    0x95, 0x10, //         REPORT_COUNT(16)
    0x75, 0x01, //         REPORT_SIZE(1)
    0x81, 0x02, //         INPUT(Data, Var, Abs)
    0x05, 0x01, //         USAGE_PAGE(Generic Desktop Controls)
    0x09, 0x30, //         USAGE(X)
    0x09, 0x31, //         USAGE(Y)
    0x15, 0x81, //         LOGICAL_MINIMUM(-127)
    0x25, 0x7F, //         LOGICAL_MAXIMUM(127)
    0x75, 0x08, //         REPORT_SIZE(8)
    0x95, 0x02, //         REPORT_COUNT(2)
    0x81, 0x02, //         INPUT(Data, Var, Abs)
    0xC0,       //     END_COLLECTION
    0xC0,       // END_COLLECTION
};

struct usb_hid_function
{
    struct usb_hid_descriptor hid_descriptor;
    struct
    {
        uint8_t bReportDescriptorType;
        uint16_t wDescriptorLength;
    } __attribute__((packed)) hid_report;
} __attribute__((packed));

static const struct usb_hid_function hid_function =
{
    .hid_descriptor = {
        .bLength = sizeof(hid_function),
        .bDescriptorType = USB_DT_HID,
        .bcdHID = 0x0100,
        .bCountryCode = 0,
        .bNumDescriptors = 1,
    },
    .hid_report = {
        .bReportDescriptorType = USB_DT_REPORT,
        .wDescriptorLength = sizeof(hid_report_descriptor),
    }
};

static const struct usb_endpoint_descriptor hid_endpoint_descriptor =
{
    .bLength = USB_DT_ENDPOINT_SIZE,
    .bDescriptorType = USB_DT_ENDPOINT,
    .bEndpointAddress = HID_ENDPOINT,
    .bmAttributes = USB_ENDPOINT_ATTR_INTERRUPT,
    .wMaxPacketSize = 8,
    .bInterval = 0x0A,
};

static const struct usb_interface_descriptor hid_interface_descriptor =
{
    .bLength = USB_DT_INTERFACE_SIZE,
    .bDescriptorType = USB_DT_INTERFACE,
    .bInterfaceNumber = 0,
    .bAlternateSetting = 0,
    .bNumEndpoints = 1,
    .bInterfaceClass = USB_CLASS_HID,
    .bInterfaceSubClass = 0,
    .bInterfaceProtocol = 0,
    .iInterface = 0,
    .endpoint = &hid_endpoint_descriptor,
    .extra = &hid_function,
    .extralen = sizeof(hid_function),
};

const struct usb_interface interfaces[] =
{
    {
        .num_altsetting = 1,
        .altsetting = &hid_interface_descriptor,
    }
};

const struct usb_config_descriptor config_descriptor =
{
    .bLength = USB_DT_CONFIGURATION_SIZE,
    .bDescriptorType = USB_DT_CONFIGURATION,
    .wTotalLength = 0,
    .bNumInterfaces = 1,
    .bConfigurationValue = 1,
    .iConfiguration = 0,
    .bmAttributes = 0xC0,
    .bMaxPower = 0x32, // TODO: 50mA, might need more when we add LED controller
    .interface = interfaces,
};

static const char *usb_strings[] =
{
    "Overkill.io",
    "USB Arcade Controller",
    "1337",
};

static int hid_control_request(usbd_device *dev, struct usb_setup_data *req, uint8_t **buf, uint16_t *len, usbd_control_complete_callback *complete)
{
    (void)complete;
    (void)dev;

    if((req->bmRequestType != 0x81) ||
       (req->bRequest != USB_REQ_GET_DESCRIPTOR) ||
       (req->wValue != 0x2200))
        return 0;

    *buf = (uint8_t*)hid_report_descriptor;
    *len = sizeof(hid_report_descriptor);

    usb_ready = true;

    return 1;
}

static void hid_set_config(usbd_device *dev, uint16_t wValue)
{
    (void)wValue;

    usbd_ep_setup(dev, HID_ENDPOINT, USB_ENDPOINT_ATTR_INTERRUPT, 8, NULL);
    usbd_register_control_callback(
        dev, USB_REQ_TYPE_STANDARD | USB_REQ_TYPE_INTERFACE,
        USB_REQ_TYPE_TYPE | USB_REQ_TYPE_RECIPIENT,
        hid_control_request);

    systick_set_clocksource(STK_CSR_CLKSOURCE_AHB_DIV8);
}

void setup_rcc(void)
{
    rcc_clock_setup_in_hse_8mhz_out_72mhz();
//    rcc_clock_setup_in_hsi_out_48mhz();

    rcc_periph_clock_enable(RCC_GPIOA);
    rcc_periph_clock_enable(RCC_GPIOB);
    rcc_periph_clock_enable(RCC_GPIOC);
    rcc_periph_clock_enable(RCC_GPIOD);
}

void setup_gpio(void)
{
    for (int i = 0; i < PIN_LAST; i++) {
        gpio_set_mode(pin_defs[i].port, GPIO_MODE_INPUT, GPIO_CNF_INPUT_PULL_UPDOWN, pin_defs[i].pin);
        gpio_clear(pin_defs[i].port, pin_defs[i].pin);
    }
}

static const uint32_t data[] =
{
    0xFF0000, 0xFF0500, 0xFF0A00, 0xFF0F00, 0xFF1400, 0xFF1900, 0xFF1F00, 0xFF2400, 0xFF2900, 0xFF2E00,
    0xFF3300, 0xFF3900, 0xFF3E00, 0xFF4300, 0xFF4800, 0xFF4D00, 0xFF5200, 0xFF5800, 0xFF5D00, 0xFF6200,
    0xFF6700, 0xFF6C00, 0xFF7200, 0xFF7700, 0xFF7C00, 0xFF8100, 0xFF8600, 0xFF8C00, 0xFF9100, 0xFF9600,
    0xFF9B00, 0xFFA000, 0xFFA500, 0xFFAB00, 0xFFB000, 0xFFB500, 0xFFBA00, 0xFFBF00, 0xFFC500, 0xFFCA00,
    0xFFCF00, 0xFFD400, 0xFFD900, 0xFFDF00, 0xFFE400, 0xFFE900, 0xFFEE00, 0xFFF300, 0xFFF800, 0xFFFE00,
    0xFAFF00, 0xF5FF00, 0xF0FF00, 0xEBFF00, 0xE5FF00, 0xE0FF00, 0xDBFF00, 0xD6FF00, 0xD1FF00, 0xCBFF00,
    0xC6FF00, 0xC1FF00, 0xBCFF00, 0xB7FF00, 0xB2FF00, 0xACFF00, 0xA7FF00, 0xA2FF00, 0x9DFF00, 0x98FF00,
    0x92FF00, 0x8DFF00, 0x88FF00, 0x83FF00, 0x7EFF00, 0x79FF00, 0x73FF00, 0x6EFF00, 0x69FF00, 0x64FF00,
    0x5FFF00, 0x59FF00, 0x54FF00, 0x4FFF00, 0x4AFF00, 0x45FF00, 0x3FFF00, 0x3AFF00, 0x35FF00, 0x30FF00,
    0x2BFF00, 0x26FF00, 0x20FF00, 0x1BFF00, 0x16FF00, 0x11FF00, 0x0CFF00, 0x06FF00, 0x01FF00, 0x00FF03,
    0x00FF08, 0x00FF0D, 0x00FF13, 0x00FF18, 0x00FF1D, 0x00FF22, 0x00FF27, 0x00FF2C, 0x00FF32, 0x00FF37,
    0x00FF3C, 0x00FF41, 0x00FF46, 0x00FF4C, 0x00FF51, 0x00FF56, 0x00FF5B, 0x00FF60, 0x00FF66, 0x00FF6B,
    0x00FF70, 0x00FF75, 0x00FF7A, 0x00FF7F, 0x00FF85, 0x00FF8A, 0x00FF8F, 0x00FF94, 0x00FF99, 0x00FF9F,
    0x00FFA4, 0x00FFA9, 0x00FFAE, 0x00FFB3, 0x00FFB8, 0x00FFBE, 0x00FFC3, 0x00FFC8, 0x00FFCD, 0x00FFD2,
    0x00FFD8, 0x00FFDD, 0x00FFE2, 0x00FFE7, 0x00FFEC, 0x00FFF2, 0x00FFF7, 0x00FFFC, 0x00FCFF, 0x00F7FF,
    0x00F2FF, 0x00ECFF, 0x00E7FF, 0x00E2FF, 0x00DDFF, 0x00D8FF, 0x00D2FF, 0x00CDFF, 0x00C8FF, 0x00C3FF,
    0x00BEFF, 0x00B8FF, 0x00B3FF, 0x00AEFF, 0x00A9FF, 0x00A4FF, 0x009FFF, 0x0099FF, 0x0094FF, 0x008FFF,
    0x008AFF, 0x0085FF, 0x007FFF, 0x007AFF, 0x0075FF, 0x0070FF, 0x006BFF, 0x0065FF, 0x0060FF, 0x005BFF,
    0x0056FF, 0x0051FF, 0x004CFF, 0x0046FF, 0x0041FF, 0x003CFF, 0x0037FF, 0x0032FF, 0x002CFF, 0x0027FF,
    0x0022FF, 0x001DFF, 0x0018FF, 0x0013FF, 0x000DFF, 0x0008FF, 0x0003FF, 0x0100FF, 0x0600FF, 0x0C00FF,
    0x1100FF, 0x1600FF, 0x1B00FF, 0x2000FF, 0x2600FF, 0x2B00FF, 0x3000FF, 0x3500FF, 0x3A00FF, 0x3F00FF,
    0x4500FF, 0x4A00FF, 0x4F00FF, 0x5400FF, 0x5900FF, 0x5F00FF, 0x6400FF, 0x6900FF, 0x6E00FF, 0x7300FF,
    0x7900FF, 0x7E00FF, 0x8300FF, 0x8800FF, 0x8D00FF, 0x9200FF, 0x9800FF, 0x9D00FF, 0xA200FF, 0xA700FF,
    0xAC00FF, 0xB200FF, 0xB700FF, 0xBC00FF, 0xC100FF, 0xC600FF, 0xCC00FF, 0xD100FF, 0xD600FF, 0xDB00FF,
    0xE000FF, 0xE500FF, 0xEB00FF, 0xF000FF, 0xF500FF, 0xFA00FF, 0xFF00FE, 0xFF00F8, 0xFF00F3, 0xFF00EE,
    0xFF00E9, 0xFF00E4, 0xFF00DF, 0xFF00D9, 0xFF00D4, 0xFF00CF, 0xFF00CA, 0xFF00C5, 0xFF00BF, 0xFF00BA,
    0xFF00B5, 0xFF00B0, 0xFF00AB, 0xFF00A5, 0xFF00A0, 0xFF009B, 0xFF0096, 0xFF0091, 0xFF008C, 0xFF0086,
    0xFF0081, 0xFF007C, 0xFF0077, 0xFF0072, 0xFF006C, 0xFF0067, 0xFF0062, 0xFF005D, 0xFF0058, 0xFF0052,
    0xFF004D, 0xFF0048, 0xFF0043, 0xFF003E, 0xFF0039, 0xFF0033, 0xFF002E, 0xFF0029, 0xFF0024, 0xFF001F,
    0xFF0019, 0xFF0014, 0xFF000F, 0xFF000A, 0xFF0005, 0xFF0000, 0xFF0500, 0xFF0A00, 0xFF0F00, 0xFF1400, 
    0xFF1900, 0xFF1F00, 0xFF2400, 0xFF2900, 0xFF2E00, 0xFF3300, 0xFF3900, 0xFF3E00, 0xFF4300, 0xFF4800, 
    0xFF4D00, 0xFF5200, 0xFF5800, 0xFF5D00, 0xFF6200, 0xFF6700, 0xFF6C00, 0xFF7200, 0xFF7700, 0xFF7C00, 
    0xFF8100, 0xFF8600, 0xFF8C00, 0xFF9100, 0xFF9600, 0xFF9B00, 0xFFA000, 0xFFA500, 0xFFAB00, 0xFFB000, 
    0xFFB500, 0xFFBA00, 0xFFBF00, 0xFFC500, 0xFFCA00, 0xFFCF00, 0xFFD400, 0xFFD900, 0xFFDF00, 0xFFE400, 
    0xFFE900, 0xFFEE00, 0xFFF300, 0xFFF800, 0xFFFE00, 0xFAFF00, 0xF5FF00, 0xF0FF00, 0xEBFF00, 0xE5FF00, 
    0xE0FF00, 0xDBFF00, 0xD6FF00, 0xD1FF00, 0xCBFF00, 0xC6FF00, 0xC1FF00, 0xBCFF00, 0xB7FF00, 0xB2FF00, 
    0xACFF00, 0xA7FF00, 0xA2FF00, 0x9DFF00, 0x98FF00, 0x92FF00, 0x8DFF00, 
};

uint32_t data_len = sizeof(data) / sizeof(uint32_t);
#define NUM_LEDS 1

int main(void)
{
    uint8_t report[5] = { 0 };
    int i;
    apa102_t apa;

    setup_rcc();
    setup_gpio();

    apa102_init(&apa, SPI2, pin_defs[PIN_SPI2_CLK], pin_defs[PIN_SPI2_MOSI]);
    apa102_setlevel(&apa, 0.1f);

    usbd_device *usbd_dev = usbd_init(&st_usbfs_v1_usb_driver, &device_descriptor, &config_descriptor, usb_strings, 3, usbd_control_buffer, sizeof(usbd_control_buffer));
    usbd_register_set_config_callback(usbd_dev, hid_set_config);
    srand(0);

    for (i = 0; i < 0x80000; i++)
        __asm__("nop");

    int idx = 0;
    while(1) {
        usbd_poll(usbd_dev);

        apa102_draw(&apa, &data[idx], 1);
        idx = (idx +1) % 357;
        //idx = (idx + 1) % (data_len - NUM_LEDS);

        for (i = 0; i < 250000; i++) {
            __asm__("nop");
        }

        if(!usb_ready)
            continue;

        memset(report, 0, sizeof(report));
        int report_byte = 1; // First byte is report ID
        for(int i = PIN_P1_UP; i <= PIN_P1_START; i++)
        {
            const int idx = i % 8;
            if(gpio_get(pin_defs[i].port, pin_defs[i].pin))
                report[report_byte] |= 1 << idx;
            else
                report[report_byte] &= ~(1 << idx);

            if(i != PIN_P1_UP && idx == 0)
                report_byte++;
        }

        report[0] = P1_REPORT_ID;
        usbd_ep_write_packet(usbd_dev, HID_ENDPOINT, report, 5);

        memset(report, 0, sizeof(report));
        report_byte = 1; // First byte is report ID
        for(int i = PIN_P2_UP; i <= PIN_P2_START; i++)
        {
            const int idx = (i - PIN_P2_UP) % 8;
            if(gpio_get(pin_defs[i].port, pin_defs[i].pin))
                report[report_byte] |= 1 << idx;
            else
                report[report_byte] &= ~(1 << idx);

            if(i != PIN_P2_UP && idx == 0)
                report_byte++;
        }

        report[0] = P2_REPORT_ID;
        usbd_ep_write_packet(usbd_dev, HID_ENDPOINT, report, 5);
    }
}

